# Práctica para acortar URLs

import webapp
import random

class randomshort(webapp.webApp):

    urls = {}  # Inicializamos el diccionario de urls que emplearemos para asignar las urls cortas a las originales

    def parse(self, request):
        """Return the resource name (including /)"""
        recurso = request.split()[1]
        print("Recurso:", recurso)
        cuerpo = request.split('\r\n\r\n')[1]
        print("Cuerpo: ", cuerpo)
        metodo = request.split(' ', 2)[0]
        print("Metodo:", metodo)

        return recurso, cuerpo, metodo

    def process(self, parsedRequest):

        recurso, cuerpo, metodo = parsedRequest

        formulario = "<p>" + "<form action='' method='POST'><p>" \
                     + "Escriba la url que desea que acortemos : <input name= 'cuerpo'>" \
                     + "<input type='submit' value='Enviar' />" \
                     + "</body></html>"

        if metodo == "GET":
            if recurso in self.urls.keys():
                httpCode = "301 Moved Permanently"
                htmlBody = "<html><body><meta charset='UTF-8'/>" \
                           + "<p>Le redirigiremos a la página " + self.urls[recurso] + " enseguida.<p>" \
                           + "<meta http-equiv = 'refresh' content='3; url=" + self.urls[recurso] + "'>" \
                           + "</body></html>"
            elif recurso == "/":
                httpCode = "200 OK"
                htmlBody = '<html><body><meta charset="UTF-8"/>' \
                           + "Bienvenido al acortador de URLS!" + "<br><br>" \
                           + "Urls acortadas previamente:<br>" + str(self.urls) + formulario \
                           + '<body><html>'
            else:
                httpCode = "404 Not Found"
                htmlBody = '<html><body><meta charset="UTF-8"/>' \
                           + "<p>El recurso " + recurso + " no se encuentra disponible<p>" \
                           + '<body><html>'
        else:
            if cuerpo.find("cuerpo=") == -1:
                httpCode = "400 Bad Request"
                htmlBody = "<html><body> Error <body><html>"

                return (httpCode, htmlBody)

            url = cuerpo.split("=")
            theurl = url[1]
            if not theurl.startswith("http://") and not theurl.startswith("https://"):
                theurl = ("https://" + theurl)
                print("URL: " + theurl)

            numero = random.randint(100, 999)
            shortedurl = "/" + str(numero)
            if theurl != "":
                self.urls[shortedurl] = theurl
                httpCode = "200 OK"
                htmlBody = '<html><body><meta charset="UTF-8"/>' \
                            + "Lista de URLS acortadas:<br>" + str(self.urls) + '<br><br>' \
                            + "URL Original: " + self.urls[shortedurl]  \
                            + "<br>" \
                            + "Url acortada: " + shortedurl \
                            + formulario + "<body><html>"

            else:
                httpCode = "404 Not Found"
                htmlBody = '<html><body><meta charset="UTF-8"/>' \
                           + "<p>ERROR<p>"

        return (httpCode, htmlBody)

if __name__ == "__main__":
    testWebApp = randomshort("localhost", 1234)